Python training UGA 2017
========================

**A training to acquire strong basis in Python to use it efficiently**

The code of this training has been written for Python 3.

To display and/or modify the presentations, use the Makefile. See::

  make help

Repositories
------------

- git: https://sourcesup.renater.fr/projects/py-training-uga/

Clone this repository with::

  git clone https://git.renater.fr/py-training-uga.git

- hg: https://bitbucket.org/fluiddyn/py-training-uga

Clone this repository with::

  hg clone https://bitbucket.org/fluiddyn/py-training-uga

Authors
-------

Pierre Augier (LEGI), Cyrille Bonamy (LEGI), Eric Maldonado (Irstea), Franck
Thollard (ISTERRE), Oliver Henriot (GRICAD), Christophe Picard (LJK)

