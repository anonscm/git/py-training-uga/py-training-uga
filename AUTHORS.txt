
The main contributors are:

- Pierre Augier (LEGI, CNRS)
- Eric Maldonado (Irstea)
- Cyrille Bonamy (LEGI, CNRS)
- Franck Thollard (ISTERRE)
- Oliver Henriot (GRICAD)
- Christophe Picard (LJK)
