#!/usr/bin/env python3
"""Computes basic statistics on file that contains a set of lines, each line
containing one float and possibly some comments in the middle of the line.

"""


def compute_stats(file_name, comment_sign):
    """
    computes the statistics of data in file_name
    :param file_name: the name of the file to process
    :type file_name: str
    :param comment_sign: the character that starts a comment
    :type comment_sign: str
    :return: the statistics
    :rtype: a tuple (number, sum, average)
    """
    total_sum = 0.0
    number = 0
    with open(file_name) as handle:
        for line in handle:
            if line.startswith(comment_sign):
                continue
            if comment_sign in line:
                idx = line.index(comment_sign)
                line = line[:idx]
            elem = float(line)
            total_sum += elem
            number += 1

    return (number, total_sum, total_sum/float(number))

file_names = ['../data/file_with_comment_col0.txt',
              '../data/file_with_comment_anywhere.txt']

numbers = []
sums = []
comment_sign = '#'

for file_name in file_names:
    (local_number, local_sum, local_avg) = compute_stats(
        file_name, comment_sign)
    numbers.append(local_number)
    sums.append(local_sum)

for (file_name, local_number, local_sum) in zip(file_names, numbers, sums):
    print('file = "{}"\nnb = {:<5}; sum = {:<7,.2f}; avg = {:<5,.2f}'.format(
        file_name, local_number, local_sum,
        local_sum/local_number))

all_sum = sum(sums)
all_numbers = sum(numbers)
print('# total over all files:\n'
      'nb = {: <5}; sum = {:<7,.2f}; avg = {:.2f}'.format(
          all_numbers, all_sum, all_sum/all_numbers))
