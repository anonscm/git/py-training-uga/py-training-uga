

path = 'data/wiki.1190153705_part_utf8'

country_stats = {}
country_stats_modif = {}

with open(path) as f:
    for line in f:
        words = line.split()
        url = words[2]
        tmp = url.split('://')[1]
        if tmp[2] == '.':
            country = tmp[:2]
            try:
                country_stats[country] += 1
            except KeyError:
                country_stats[country] = 1

        print(words[-1])

print(sorted(country_stats.items(), key=lambda x: x[1])[::-1])
