
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import cm

import geojson
from descartes import PolygonPatch

from load_results import votes, keys, names, percentages

name = 'BUREAUX_DE_VOTE_EPSG4326.json'

with open(name) as f:
    bureaux = geojson.load(f)

colormaps = {'MELENCHON': cm.OrRd,
             'MACRON': cm.Oranges,
             'FILLON': cm.Blues,
             'LE PEN': cm.Greys,
             'HAMON': cm.PuRd}


def plot(key, vote=True):

    colormap = colormaps[key]

    title = key
    if vote:
        datas = votes
        title += ' (nombre de votants)'
    else:
        datas = percentages
        title += ' (%)'

    index_key = keys.index(key)
    norm = datas[:, index_key].max()

    # compute colors for each bureau
    colors = {}
    for iname, name in enumerate(names):
        index = int(name.split()[0])
        data = datas[iname, index_key]
        colors[index] = colormap(data/norm)

    polygons = []

    for bureau in bureaux['features']:
        index = bureau['properties']['SDEC_NUM']
        # bureau['properties']['SDEC_LIBEL']
        color = colors[index]
        polygons.append(
            PolygonPatch(
                bureau['geometry'],
                fc=color, ec='k', alpha=0.8, zorder=2))

    fig = plt.figure()

    ax = fig.gca()
    if not vote:
        norm *= 100
    ticks = np.linspace(0, norm, 5)
    tmp = np.ones(len(ticks))
    sc = ax.scatter(tmp, tmp, c=ticks, cmap=colormap)
    ax.cla()

    for polygon in polygons:
        ax.add_patch(polygon)
    ax.axis('scaled')

    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])

    ax.set_title(title)

    plt.colorbar(sc, ax=ax)
    plt.tight_layout()


if __name__ == '__main__':

    for key in colormaps.keys():
        plot(key)
        plot(key, False)

    plt.show()
