
import numpy as np

# see https://bitbucket.org/openpyxl/openpyxl
from openpyxl import load_workbook

path = 'resultats_presidentielle_2017_1er_tour_Grenoble.xlsx'

workbook = load_workbook(path)

sheet = workbook.get_sheet_by_name('Sheet1')

keys = []
columns = {}
indices_columns = []

names = []

for row in sheet.iter_rows():
    cell = row[0]
    if cell.row == 8:
        for cell in row:
            if cell.value is not None:
                key = cell.value.strip()
                keys.append(key)
                columns[key] = cell.column
                indices_columns.append(cell.col_idx-1)
    elif cell.row > 8:
        name = row[1].value
        if name is not None:
            name = name.strip()
            if name != 'TOTAL':
                names.append(name)

nb_bureaux = len(names)
nb_values = len(keys)

results = np.zeros([nb_bureaux, nb_values], dtype=int)
i0 = 0

for row in sheet.iter_rows():
    cell = row[0]
    if cell.row > 8:
        name = row[1].value
        if name is not None:
            name = name.strip()
            if name == 'TOTAL':
                continue
            i1 = 0
            for index in indices_columns:
                value = row[index].value
                if value is None:
                    value = 0
                results[i0, i1] = int(value)
                i1 += 1
            i0 += 1

index_expr = keys.index('Exprimés')
expr = results[:, index_expr]

percentages = np.empty(results.shape, dtype=float)

for i1 in range(results.shape[1]):
    percentages[:, i1] = results[:, i1] / expr

votes = results
