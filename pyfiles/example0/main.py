# 3 different syntaxes for importing a module
import util
import util as u
from util import b, print_ab

u.a = 100
b += 100
print('util.a = {}; u.b = {}; b = {}'.format(util.a, u.b, b))
print_ab()

print('in main.py, __name__ =', __name__)
if __name__ == '__main__':
    print('end of main.py')
